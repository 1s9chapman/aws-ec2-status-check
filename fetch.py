# import boto3 and Slack from slack_webhook
import boto3
from slack_webhook import Slack

# Define boto3.client ec2 module, and slack webhook module
client = boto3.client('ec2')
slack = Slack(url='https://hooks.slack.com/services/replace_with_hook_id')

# send api call to pull instance reachability
response = client.describe_instance_status(
    Filters=[
        {
            'Name': 'instance-status.status',
            'Values': [
                'ok'
            ],
            'Name': 'instance-status.reachability',
            'Values': [
                'failed'
            ]
        }
    ]
)

# define response with InstanceStatuses dict
fetch = response['InstanceStatuses']

# if statement to query each instance for a failed reachability status
if(fetch == []):
    print("No failed instances")

# if fetch contains data loop response in for loop through list of failed instances and return each to slack channel
else:
    print("Fail instance")
    for id in response["InstanceStatuses"]:
        instance_id = id["InstanceId"]
        slack.post(text='Instance ' + instance_id + ' Failed Status check')